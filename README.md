# Cracking Coding Solutions

Idea behind this repository is to pick a Topic in Cracking Coding Interview Book and Solve the Coding Problems,  and create a Pull Request to this repository. The main idea is to learn ,teach,discuss,collaborate the ideas, this way everyone in this forum gets better.

A Pull Request should contain

Explanation: the Explanation of Logic,Briefly

**Time Complexity**: O(n), O(n <sup> 2 </sup>), O(n log n), O(log n), O(n x m) etc

**Space Complexity**: O(n), O(n <sup> 2 </sup>), O(n log n), O(log n), O(n x m) etc
Notify: 

Tag the teammates so that they are notified about the PR
More Information: Any other information, you learnt during implementing the logic, you think would be useful for this forum
We will create a subpackage ,For Eg: For String..com.crackthecodes.strings.
Your Solution Python file should follow naming convention <ProblemNumber><NameOfProblem> under the corresponding package.

For Eg: If we are solving duplicateliterals problem from Strings Chapter,first create a package com.crackthecodes.strings.
The file name should be **P001_DuplicateLiterals.py**

Create a base Test case in test folder following the same package and file conventions with ending with name Test. 
For Eg: under test folder:-
com.crackthecodes.string
**P001_DuplicateLieteralsTest.py**

Happy Coding!